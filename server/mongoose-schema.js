const mongoose = require("mongoose");

const cardDetailsSchema = new mongoose.Schema(
  {
    cardNumber: {
      type: String,
      maxLength: 16,
      required: true,
    },
    cardType: {
      type: String,
      required: true,
    },
    expiryMonth: {
      type: Number,
      required: true,
    },
    expiryYear: {
      type: Number,
      required: true,
    },
    gaps: {
      type: [Number],
      required: true,
    },
    cvv: {
      type: Number,
      minLength: 3,
      maxLength: 3,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);


module.exports = mongoose.model('CardDetails', cardDetailsSchema);