const express = require("express");
const cors = require("cors");
const path = require("path");
const mongoose = require("mongoose");
const app = express();

const CardDetails = require("./mongoose-schema");
const distPath = path.join(__dirname, "../dist/saved-cards");
console.log(distPath);

app.use(express.json());
app.use(cors());
app.use(express.static(distPath));

//---------- Handlers ------------
const saveCardDetails = async (req, res) => {
  try {
    console.log("here");
    let { cardNumber } = req.body;
    //Remove spaces
    cardNumber = cardNumber.split(" ").join("");
    req.body.cardNumber = cardNumber;
    const card = await CardDetails.findOne({ cardNumber });
    if (card) throw "Card already exists";
    const cardCreated = await CardDetails.create(req.body);
    if (!cardCreated) throw "Error in saving card details";
    res.status(201).json({ message: "Card details saved successfully" });
  } catch (err) {
    console.log(err);
    res.status(400).json({ error: err || "Some error occurred" });
  }
};

const getCardDetails = async (req, res) => {
  try {
    const { cardType } = req.params;
    const query = cardType ? (cardType !== "all" ? { cardType } : {}) : {};
    const savedCards = await CardDetails.find(query).sort({ createdAt: -1 });
    if (!savedCards) throw "No saved cards";
    res.send(savedCards);
  } catch (err) {
    res.status(400).json({ error: err || "Some error occurred" });
  }
};

const removeCard = async (req, res) => {
  try {
    const { id } = req.params;
    const card = await CardDetails.findById(id);
    if (!card)
      return res.json({ message: "Card does not exist in the database" });
    const result = await CardDetails.deleteOne({ _id: id });
    if (!result) throw "Unable to delete card";
    res.json({ message: "Card successfully deleted" });
  } catch (err) {
    res.status(400).json({ error: err || "Some error occurred" });
  }
};
//--------------------------------

//--------- Routes --------------
app.get("/api/getSavedCards/:cardType?", getCardDetails);
app.post("/api/saveCard", saveCardDetails);
app.delete("/api/deleteCard/:id", removeCard);
//-------------------------------

//----------Mongoose config---------------
const mongoURI =
  "mongodb+srv://saveCardDB:mSR6ZbOyMEcDy4rj@cluster0.idn3j.mongodb.net/SaveCardDetails?retryWrites=true&w=majority";
mongoose.connect(mongoURI, (err) => {
  if (err) {
    console.log("Error in connecting to MongoDB server", err);
  } else {
    console.log("Database Connected");
  }
});
//-----------------------------------------

const PORT = process.env.PORT;

app.listen(PORT || 4000, () => {
  console.log(`Server listening on ${PORT}`);
});
