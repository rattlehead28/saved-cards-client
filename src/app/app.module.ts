import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CreditCardDirectivesModule } from 'angular-cc-library';
import { AppComponent } from './app.component';
import { CardsComponent } from './cards/cards.component';
import { AddCardComponent } from './cards/add-card/add-card.component';
import { CardNumberMaskPipe } from './pipes/card-number-mask.pipe';
import { HeaderComponent } from './shared/header/header.component';
import { AcceptNumberDirective } from './directives/accept-number.directive';
import { NgxSpinnerModule } from 'ngx-spinner';
@NgModule({
  declarations: [
    AppComponent,
    CardsComponent,
    AddCardComponent,
    CardNumberMaskPipe,
    HeaderComponent,
    AcceptNumberDirective,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CreditCardDirectivesModule,
    NgxSpinnerModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
