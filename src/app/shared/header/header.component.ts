import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SharedService } from 'src/app/core/shared.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  @ViewChild('cardFilter') cardFilter: ElementRef; //Reference to 'select' tag of filter
  constructor(private sharedService: SharedService) {}

  ngOnInit(): void {}

  ngAfterViewInit(): void{
    //Subscription to update the selected value in filter
    this.sharedService.updateFilter.subscribe( type => this.cardFilter.nativeElement.value=type);
  }

  //Function to set the selected card type in filter in the shared service
  filterCardList(cardType): void{
    this.sharedService.setCardType(cardType);
  }
}
