export interface ICardDetail {
    "cardNumber": string;
    "cardType": string;
    "expiryMonth": string;
    "expiryYear": string;
    "cvv": string;
    "gaps":[number]
}