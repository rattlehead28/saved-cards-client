//Creating the arrays of valid months and years to loop through
//using *ngFor when rendering dropdowns for expiry month and year

//months -> numbers from 1 to 12
//years -> 10 years including the current year
export const expiryDate = {
    get months(){
        return Array(12).fill(1).map((e,i)=> e*i+1)
    },
    get years(){
        return Array(10).fill(1).map((e,i)=> e*(new Date().getFullYear()+i))
    }
}