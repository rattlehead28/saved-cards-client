const basePath="assets/card_logos/"
//image path for allowed card types
export const cardLogoSrc = {
    "visa": basePath+"visa.png",
    "mastercard": basePath+"mastercard.png",
    "amex": basePath+"amex.png"
}