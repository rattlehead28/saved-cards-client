import { Directive, HostListener } from '@angular/core';
const allowedKeys = [
  '0',
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '9',
  'Backspace',
  'Delete',
  'ArrowRight',
  'ArrowLeft',
];
@Directive({
  selector: '[appAcceptNumber]',
})
export class AcceptNumberDirective {
  constructor() {}
  @HostListener('keydown', ['$event'])
  keyDown(evt) {
    if (allowedKeys.indexOf(evt.key) > -1) {
      return true;
    }
    return false;
  }
}
