import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { catchError } from 'rxjs/operators';
const { baseAPI } = environment;

@Injectable({
  providedIn: 'root',
})
export class CardApiService {
  constructor(private http: HttpClient) {}

  saveCardDetails(cardDetails) {
    return this.http.post(`${baseAPI}saveCard`, cardDetails).pipe(
      catchError((err: HttpErrorResponse) => {
        const errObj = err.error;
        if (errObj && typeof errObj.error === 'string') {
          throw errObj.error;
        }
        throw 'Some error occurred';
      })
    );
  }

  getCardDetails(cardType?: string) {
    const url = cardType ? `getSavedCards/${cardType}` : `getSavedCards`;
    return this.http.get(`${baseAPI}${url}`).pipe(
      catchError((err: HttpErrorResponse) => {
        const errObj = err.error;
        if (errObj && typeof errObj.error === 'string') {
          throw errObj.error;
        }
        throw 'Some error occurred';
      })
    );
  }

  removeCard(id) {
    return this.http.delete(`${baseAPI}deleteCard/${id}`).pipe(
      catchError((err: HttpErrorResponse) => {
        const errObj = err.error;
        if (errObj && typeof errObj.error === 'string') {
          throw errObj.error;
        }
        throw 'Some error occurred';
      })
    );
  }
}
