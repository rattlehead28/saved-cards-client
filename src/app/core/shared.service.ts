import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SharedService {
  cardFilter = new BehaviorSubject<string>("all");
  updateFilter = new BehaviorSubject<string>("all");
  constructor() {}

  //Function called by 'Header' component to notify 'CardComponent' about the filter selected
  setCardType(type) {
    this.cardFilter.next(type);
  }

  //Function called by 'CardComponent' to sync the filter with the cards rendered in the 'CardComponent'
  updateCardFilter(type){
    this.updateFilter.next(type);
  }
}
