import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'maskCardNo',
})
export class CardNumberMaskPipe implements PipeTransform {
  transform(cardNo: string, gaps: [number]) {
    if (cardNo) {
      const maskedStr = cardNo.split('').map((elem, i) => {
        if (i > 3 && i < cardNo.length - 4) {
          return 'X';
        } else return elem;
      });
      gaps.map((elem) => {
        maskedStr.splice(elem, 0, ' ');
      });
      return maskedStr.join('');
    }
  }
}

//Transformations achieved:-
//  1. All numbers are replaced by 'X' excluding first and last four numbers
//  2. Spaces are introduced based on the 'gaps' array. The spaces introduced
//     can vary based on card type
