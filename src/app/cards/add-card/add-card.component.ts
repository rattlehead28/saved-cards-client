import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter,
} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { cardLogoSrc } from '../../constants/card-src';
import { expiryDate } from '../../constants/expiry-date';
import { allowedCardTypes } from '../../constants/allowed-cards';
import { CreditCardValidators } from 'angular-cc-library';
import { CardApiService } from 'src/app/core/card-api.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-add-card',
  templateUrl: './add-card.component.html',
  styleUrls: ['./add-card.component.css'],
})
export class AddCardComponent implements OnInit {
  @ViewChild('modalButton') modalButton: ElementRef; //open modal button reference
  @ViewChild('ccNumber') ccNumber; //access the cc library directive
  @ViewChild('closeFormModal') closeFormModal: ElementRef; //close modal button reference
  @Output() cardAdded = new EventEmitter(); //event emitted to parent component on successfull submission of card details
  cardData;
  imgSrc: string;
  isDateValid: boolean = true;
  expiryDateObj = expiryDate;
  ccNumberSub: Subscription;
  showAlert: boolean = false;
  errorMessage: string;

  constructor(
    private fb: FormBuilder,
    private cardApiService: CardApiService,
    private spinner: NgxSpinnerService
  ) {
    //reference of component is binded in order to access 'cardData' in 'checkOnAllowedCardTypes' function
    this.checkOnAllowedCardTypes = this.checkOnAllowedCardTypes.bind(this);
    //build form named 'cardData'
    this.cardData = fb.group({
      cardNumber: ['', [CreditCardValidators.validateCCNumber]],
      cardType: [
        'unknown',
        [Validators.required, this.checkOnAllowedCardTypes],
      ],
      expiryMonth: ['', Validators.required],
      expiryYear: ['', Validators.required],
      cvv: ['', Validators.required],
      gaps: [[]],
    });
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.ccNumberSub = this.ccNumber.resolvedScheme$.subscribe((res) => {
      this.cardData.patchValue({ cardType: res });
      this.imgSrc = cardLogoSrc[res];
    });
  }

  //custom validator to include only allowed card types
  checkOnAllowedCardTypes(control) {
    if (this.cardData) {
      if (allowedCardTypes.indexOf(control.value) === -1) {
        this.cardData.get('cardNumber').setErrors({ ccNumber: true });
      }
    }
  }

  //function to check if the expiry month and year are greater than or equal to current month and year
  expiryDateValidation(): void {
    const selectedMonth = this.cardData.controls['expiryMonth'].value;
    const selectedYear = this.cardData.controls['expiryYear'].value;
    if (selectedMonth && selectedYear) {
      const currentMonth = new Date().getMonth() + 1;
      const currentYear = new Date().getFullYear();
      this.isDateValid =
        selectedYear > currentYear
          ? true
          : selectedMonth < currentMonth
          ? false
          : true;
      if (!this.isDateValid) {
        this.cardData.get('expiryMonth').setErrors({ isValid: true });
        this.cardData.get('expiryYear').setErrors({ isValid: true });
      } else {
        this.cardData.get('expiryMonth').setErrors(null);
        this.cardData.get('expiryYear').setErrors(null);
      }
    }
  }

  //Function to keep a record of gaps introduced while entering card number so that gaps can be re-introduced
  //when displaying card details in the list
  recordGaps() {
    let cardNo = this.cardData.get('cardNumber').value;
    let gaps = [];
    if (cardNo) {
      cardNo.split('').map((elem, i) => {
        if (elem == ' ') {
          gaps.push(i);
        }
      });
      this.cardData.patchValue({
        gaps: gaps,
      });
    }
  }

  //Function to submit card details filled by the user
  //POST api call is hit to save the card details in MongoDB
  submitCardDetails(): void {
    this.recordGaps();
    this.spinner.show();
    this.cardApiService.saveCardDetails(this.cardData.value).subscribe(
      (res: any) => {
        this.spinner.hide();
        if (res && res.message) {
          this.cardData.reset();
          this.imgSrc="";
          this.cardAdded.emit();
          this.closeFormModal.nativeElement.click();
        }
      },
      (err) => {
        this.spinner.hide();
        this.manageAlert(true, err);
      }
    );
  }

  //Alert is visible for 4 seconds
  manageAlert(show, message) {
    this.showAlert = show;
    this.errorMessage = message;
    setTimeout(() => (this.showAlert = false), 4000);
  }

  ngOnDestroy() {
    this.ccNumberSub.unsubscribe();
  }
}
