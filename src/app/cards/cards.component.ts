import {
  Component,
  OnInit,
  ViewChild,
  ElementRef
} from '@angular/core';
import { AddCardComponent } from './add-card/add-card.component';
import { cardLogoSrc } from '../constants/card-src';
import { CardApiService } from '../core/card-api.service';
import { ICardDetail } from '../constants/interfaces';
import { Subject, Subscription } from 'rxjs';
import { SharedService } from '../core/shared.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css'],
})
export class CardsComponent implements OnInit {
  @ViewChild('confirmDialog') confirmDialog: ElementRef;
  @ViewChild(AddCardComponent) addCard: AddCardComponent; //reference of the child component 'AddCardComponent'
  @ViewChild('confirmDialogModalButton') confirmDialogModalButton: ElementRef; //reference of the button that open confirm dialog modal
  @ViewChild('closeModal') closeModal: ElementRef; //close confirm dialog modal button reference
  cardSrc = cardLogoSrc; //source of logo of allowed card types is present in cardLogoSrc
  cardLists: [ICardDetail]; //saved cards
  confirmClicked$: Subject<boolean> = new Subject(); //subject is created which emits value when user confirms deletion
  modalClickedSub: Subscription;
  cardListSub: Subscription;

  cardType: string; //to record which card type is selected in filter
  showAlert: boolean = false;
  errorMessage: string;
  constructor(
    private cardApiService: CardApiService,
    private sharedService: SharedService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    //subscription to sync with the filter selected in 'Header' component
    this.sharedService.cardFilter.subscribe((type) => {
      this.cardType = type;
      this.getCardList(type);
    });
  }

  //Function to get all the saved cards based on the filter selected
  //If called with no arguments then 'all' is passed to the api to get
  //all the saved cards
  getCardList(type?) {
    this.spinner.show();
    this.cardApiService.getCardDetails(type).subscribe(
      (res: [ICardDetail]) => {
        this.spinner.hide();
        this.sharedService.updateCardFilter(type || 'all');
        this.cardLists = res;
      },
      (err) => {
        this.spinner.hide();
        this.manageAlert(true, err);
      }
    );
  }

  //Function to call delete api with DB id of the card to be removed
  removeCard(id) {
    this.confirmDialogModalButton.nativeElement.click(); //confirm modal is opened
    //Subscription to the click event of the confirm modal
    this.modalClickedSub = this.confirmClicked$.subscribe((isConfirm) => {
      if (isConfirm) {
        this.spinner.show();
        //User clicked on 'OK' to confirm deletion
        this.cardListSub = this.cardApiService.removeCard(id).subscribe(
          (res: any) => {
            this.spinner.hide();
            if (res && res.message) {
              //Get saved cards on the basis of filter in 'Header' component
              this.getCardList(this.cardType);
              this.dialogModalClosed();
              this.closeModal.nativeElement.click();
            }
          },
          (err) => {
            this.spinner.hide();
            this.dialogModalClosed();
            this.closeModal.nativeElement.click();
            this.manageAlert(true, err);
          }
        );
      }
    });
  }

  dialogModalClosed() {
    this.modalClickedSub.unsubscribe();
    this.modalClickedSub.unsubscribe();
  }

  manageAlert(show, message) {
    this.showAlert = show;
    this.errorMessage = message;
    setTimeout(() => (this.showAlert = false), 4000);
  }

  showModal() {
    this.addCard.modalButton.nativeElement.click();
  }
}
